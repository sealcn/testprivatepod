//
//  ViewController.swift
//  TestPod
//
//  Created by RentonLIn on 2018/11/21.
//  Copyright © 2018 Renton. All rights reserved.
//

import UIKit
import MVABTest

class ViewController: UIViewController {

    @IBOutlet weak var exampleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //初始化Firebase,然后设置ABTest属性
//        FirebaseApp.configure()
        //!!!!!!!!!!!!!!!
        //这一块的代码和Firebase密切相关，所以没有封装在ABTest库里面
//        Analytics.setUserProperty(MVABTest.shared.getGroupID(), forName: "MVABTest.firebaseUserPropertyGroupIDKey)
//        Analytics.setUserProperty("en", forName: MVABTest.firebaseUserPropertyLanguageKey)
        //初始化
        MVABTest.initial(appBundleId: "com.daily.horoscope.free",
                         version: "3.3.5",
                         countryCode: "CN",
                         language: "en")
        //设置默认值
        MVABTest.shared.setDefaultFeatureConfig(defaultConfig: [ABTestEnum.homeButtonTitle.rawValue : "DefaultTitle"])
        //获取当前的值
        let homeButtonTitle = MVABTest.shared.getFeatureABTestValueForKey(key: ABTestEnum.homeButtonTitle.rawValue)
        print(homeButtonTitle ?? "nil")
        self.exampleButton.setTitle(homeButtonTitle, for: UIControl.State.normal)
        
        //监听更新
        let _ = MVABTest.shared.getUpdateObservable().subscribe(onNext: { [weak self] value in
            print("get value from MVABTest")
            //获取新的值,会触发最多两次, 第一次是缓存里面的值，第二次是更新后的值
            let homeButtonTitle = MVABTest.shared.getFeatureABTestValueForKey(key: ABTestEnum.homeButtonTitle.rawValue)
            print(homeButtonTitle ?? "nil")
            self?.exampleButton.setTitle(homeButtonTitle, for: UIControl.State.normal)
        })
    }


}

